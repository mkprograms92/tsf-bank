<!DOCTYPE html>
<html>
<head>
		<title>TSF Bank</title>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  		<link rel="stylesheet" type="text/css" href="css/style1.css">	
      <style type="text/css">
        *{
  padding: 0;
  margin: 0;
  font-family: sans-serif;
}
.container
{
    position: relative;
}
.center
{
  position: absolute;
    top:40%;
    left:60%;
    transform: translate(-50%, -50%);
    font-family: sans-serif;    
}
.ab
{
  font-weight: bold;
  text-align: center;
  margin-right:56%;

}
.wave
{
  width:45%;  
  margin-top: 20px;
  padding-top: 50px;

}


div.absolute {
  position: absolute;
  width: 50%;
  top: 73%;
  bottom: 0;
  margin-left:25%;
} 
  
      </style>

</head>
<body>
 <?php
      include 'navbar.php';
 ?>

 <div class="container">

  <img class="wave" src="img/img5.png">

  	<div class="center">
  		<h1 class="ab">Welcome to</h1>
  		<h1 class="effect-underline">The Sparks Foundation Bank</h1><br><br>
      <a href="customer.php" style="margin-left:0.5%; "><button class="btn btn-default btn-lg" style=" font-weight:bold;">View Customer</button></a>
      <a href="history.php" style="margin-left: 5%;"><button class="btn btn-default btn-lg" style=" font-weight:bold;">History</button></a>
    </div>
</div>


<div class="absolute">        
	<footer class="text-center mt-5 py-2" style="margin-top:20%;"> 
        <p><b>The Sparks Foundation Project</b> 2021 Made by <b>Mahesh Kshirsagar</b></p>
  </footer>
</div>
</body>
</html>